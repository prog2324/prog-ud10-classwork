package es.cipfpbatoi.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Act02Controller {
	@ResponseBody
	@GetMapping("/saluda")
	public String getSaludo() {
		return "<html><body><h1>Hola Mundo</h1></body></html>";
	}
	
	@ResponseBody
	@GetMapping("/bingo")
	public String getAleatorio() {
		int numero = (new Random()).nextInt(100);
		return "<html><body><h1> Número: " + numero + "</h1></body></html>";
	}
	
	@ResponseBody
	@GetMapping("/fecha-actual-es")
	public String getFechaActualEs() {
		LocalDateTime fechaHora = LocalDateTime.now();
		DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-mm-yyyy hh:mm:ss");
		return fechaHora.format(f);
	}
	
	@ResponseBody
	@GetMapping("/fecha-actual-en")
	public String getFechaActualEn() {
		LocalDateTime fechaHora = LocalDateTime.now();
		DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-mm-dd hh:mm:ss");
		return fechaHora.format(f);
	}
	
	
}

