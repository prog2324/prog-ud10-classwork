package es.cipfpbatoi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EjemploTablaMultController {
	
	@GetMapping("/enlacestablas")
	public String tablaEnlaces() {
		return "tablamult_enlaces_view";
	}
	
	@GetMapping("/formtablas")
	public String tablaFormViewAction() {
		return "tablamult_form_view";
	}
	
	@GetMapping("/tablamult")
	@ResponseBody
	public String getTablaMultiplicar(@RequestParam int num, @RequestParam String nombre) {
		StringBuilder htmlResponse = new StringBuilder();
		htmlResponse.append("Hola " + nombre + "<br>");
		htmlResponse.append("Tabla de multiplicar del " + num + "<br>");
		htmlResponse.append("------------------------------------<br>");
		for (int i = 1; i <= 10; i++) {
			htmlResponse.append(String.format("%d x %d = %d<br>",  num, i, num * i));
		}
		return htmlResponse.toString();
	}
}
