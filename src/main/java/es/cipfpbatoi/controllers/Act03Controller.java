package es.cipfpbatoi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Act03Controller {

	@GetMapping("/suma")
	@ResponseBody
	public String suma(@RequestParam int num1, @RequestParam int num2) {
		StringBuilder respuesta = new StringBuilder();
		respuesta.append("<html><body><h1>");
		respuesta.append(String.format("%d + %d = %d", num1, num2, num1 + num2));
		respuesta.append("</h1></body></html>");
		return respuesta.toString();
	}

	@GetMapping("/multiplica")
	@ResponseBody
	public String multiplica(@RequestParam int num1, @RequestParam int num2) {
		StringBuilder htmlResponse = new StringBuilder();
		htmlResponse.append("<html><body><h1>");
		htmlResponse.append(String.format("%d * %d = %d", num1, num2, num1 * num2));
		htmlResponse.append("</h1></body></html>");
		return htmlResponse.toString();
	}

	@GetMapping("/resta")
	@ResponseBody
	public String resta(@RequestParam int num1, @RequestParam int num2) {
		StringBuilder htmlResponse = new StringBuilder();
		htmlResponse.append("<html><body><h1>");
		htmlResponse.append(String.format("%d - %d = %d", num1, num2, num1 - num2));
		htmlResponse.append("</h1></body></html>");
		return htmlResponse.toString();
	}

	@GetMapping("/calculadora")
	public String calculadora() {
		return "calculadora";
	}

}
